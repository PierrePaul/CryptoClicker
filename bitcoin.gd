extends Node2D

onready var player_vars = get_node("/root/player")
	
func _process(delta):
	$money.text = str(player_vars.money)

func _on_staff_scene_btn_pressed():
	get_tree().change_scene('res://item/bitcoin/staff.tscn')
