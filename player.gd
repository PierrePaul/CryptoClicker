extends Node
var money = 10000.0
var staff = []
var item_count = {}

func has_staff(name):
	if name in staff:
		return true
		
	return false
	
func save_item_count(item_name, count):
	item_count[item_name] = count
	
func get_item_count(item_name):
	if item_name in item_count:
		return item_count[item_name]
	return 0
