extends Node2D

var level = 1
var difficulty = 1
var item_count = 0

onready var player_vars = get_node("/root/player")

var ITEM_NAME = 'item0'
var INITIAL_PRICE = 10.0
var COST_MULTIPLIER = 1.05
var INITIAL_WAIT_TIME = 10
var MAX_LEVEL = 10
var MAX_ITEMS_COUNT = 100
var STAFF_NAME = 'must_implement'


func _ready():
	$advanced_controls/level_progress_bar.max_value = (MAX_ITEMS_COUNT/MAX_LEVEL)
	$advanced_controls/btn_buy_item/btn_buy_item_label.text = "Buy at %10.2f" % get_price()
	$btn_buy_Item_first/item_initial_price.text = str(INITIAL_PRICE)
	$btn_buy_Item_first/item_name.text = ITEM_NAME
	$advanced_controls/btn_generate/item_name.text = ITEM_NAME
	item_count = player_vars.get_item_count(ITEM_NAME)
	if item_count > 0:
		$advanced_controls.visible = true
		$btn_buy_Item_first.visible = false		
	else:
		$advanced_controls.visible = false
		$btn_buy_Item_first.visible = true
	

func _exit_tree():
	player_vars.save_item_count(ITEM_NAME, item_count)
	
func _process(delta):
	if player_vars.money < get_price():
		$advanced_controls/btn_buy_item.disabled = true
	else:
		$advanced_controls/btn_buy_item.disabled = false

	if $timer.is_stopped() == false:
		var percent = (($timer.wait_time - $timer.time_left)/$timer.wait_time)*100
		$advanced_controls/progress_bar.value = percent
		$advanced_controls/timer_container/timer_label.text = transform_seconds_to_time($timer.time_left)

	else:
		$advanced_controls/timer_container/timer_label.text = '00:00:00'
		$advanced_controls/progress_bar.value = 0		
		if player_vars.has_staff(STAFF_NAME) and item_count > 0:
			_on_btn_generate_pressed()

func transform_seconds_to_time(seconds):
	var minutes = seconds/60
	var hours = minutes/60
	var days = hours/24
	return '%02d:%02d:%02d' % [hours, fmod(minutes, 60), fmod(seconds, 60)+1]


func _on_btn_generate_pressed():
	if $timer.is_stopped():
		$timer.wait_time = get_wait_time()
		$timer.start()
		$advanced_controls/btn_generate.disabled = true


func _on_timer_timeout():
	player_vars.money += get_money_return()
	$advanced_controls/btn_generate.disabled = false
	emit_signal("item_timeout")


func _on_btn_buy_item_pressed():
	if(player_vars.money >= get_price()):
		$advanced_controls.visible = true
		player_vars.money -= get_price()
	
		$btn_buy_Item_first.visible = false
		item_count += 1
		$advanced_controls/progress_bar/revenue.text = str(get_money_return())
		$advanced_controls/btn_buy_item/btn_buy_item_label.text = "Buy at %7.2f" % get_price()
		$advanced_controls/level_progress_bar/item_quantity.text = str(item_count)
		if item_count > 0 and $advanced_controls/btn_generate.disabled and $timer.is_stopped():
			$advanced_controls/btn_generate.disabled = false
		
		if item_count % (MAX_ITEMS_COUNT/MAX_LEVEL) == 0:
			level += 1
			$advanced_controls/progress_bar/revenue.text = str(get_money_return())
			$advanced_controls/level_progress_bar.value = 0
			if level == MAX_LEVEL+1:
				$advanced_controls/btn_buy_item.disabled = true
		else:
			$advanced_controls/level_progress_bar.value = (item_count % (MAX_ITEMS_COUNT/MAX_LEVEL))


func _on_btn_reset_pressed():
	$timer.stop()


func get_wait_time():
	var time = -log(float(level)) * MAX_LEVEL + INITIAL_WAIT_TIME
	if time < 1:
		time = 1
	return time


func get_money_return():
	return 5*item_count*(0.2*INITIAL_PRICE)


func get_price():
	return round(float(INITIAL_PRICE*(pow(COST_MULTIPLIER, item_count)))*10)/10
