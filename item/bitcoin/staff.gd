extends Node2D

onready var player_vars = get_node("/root/player")

func _ready():
	
	if player_vars.money < 100:
		$staff_1.disabled = true
	else:
		$staff_1.disabled = false

	if player_vars.money < 200:
		$staff_2.disabled = true
	else:
		$staff_2.disabled = false
		
	if player_vars.has_staff('staff_1'):
		$staff_1.disabled = true
	if player_vars.has_staff('staff_2'):
		$staff_2.disabled = true
		
func _process(delta):
	$player_money.text = str(player_vars.money)

func _on_staff_1_pressed():
	if player_vars.money >= 100:
		player_vars.money -= 100
		player_vars.staff.append('staff_1')
		$staff_1.disabled = true


func _on_staff_2_pressed():	
	if player_vars.money >= 200:
		player_vars.money -= 200
		player_vars.staff.append('staff_2')
		$staff_2.disabled = true

func _on_close_pressed():
	get_tree().change_scene("res://bitcoin.tscn")
